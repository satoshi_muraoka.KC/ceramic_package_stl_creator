#!/bin/bash

height=5


declare -a sides=(
  "12:12" # 1:1
  "12:24" # 1:2
  "8:24"  # 1:3
  "6:24"  # 1:4
  "4:20"  # 1:5
  "4:24"  # 1:6
  "3:21"  # 1:7
  "3:24"  # 1:8
)

# 'output'ディレクトリが存在する場合は削除し、新規作成する
output="output"
if [ -d ${output} ]; then
  rm -rf ${output}
fi
mkdir ${output}

# 配列から short_side と long_side を読み取り、それぞれのペアでコマンドを実行
for side in "${sides[@]}"
do
  IFS=':' read -ra SIZE <<< "$side"
  cmd="python3 ceramic_package_stl_creator.py\
   ${SIZE[0]}\
   ${SIZE[1]}\
   ${height}\
   ${output}\
  "

  echo $cmd
  eval $cmd
done