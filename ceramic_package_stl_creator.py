#!/usr/bin/env python
import argparse
import numpy as np
import os
import shutil

from stl import mesh


def main():
    args = parse_args()
    half_long_side = args.long_side / 2
    half_short_side = args.short_side / 2


    # Define the 8 vertices of the cube
    # [x, y, z]
    vertices = np.array([
        # 底面
        [-half_long_side, -half_short_side, 0],
        [+half_long_side, -half_short_side, 0],
        [+half_long_side, +half_short_side, 0],
        [-half_long_side, +half_short_side, 0],

        # 天面
        [-half_long_side, -half_short_side, args.height],
        [+half_long_side, -half_short_side, args.height],
        [+half_long_side, +half_short_side, args.height],
        [-half_long_side, +half_short_side, args.height]
    ])

    # Define the 12 triangles composing the cube
    faces = np.array([
        [0,3,1],
        [1,3,2],
        [0,4,7],
        [0,7,3],
        [4,5,6],
        [4,6,7],
        [5,1,2],
        [5,2,6],
        [2,3,6],
        [3,7,6],
        [0,1,5],
        [0,5,4]
    ])

    # Create the mesh
    cube = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))

    for i, f in enumerate(faces):
        for j in range(3):
            cube.vectors[i][j] = vertices[f[j],:]

    # Write the mesh to file "cube.stl"
    cube.save(f"{args.output}/cube_{args.short_side}x{args.long_side}x{args.height}.stl")


def parse_args():
    parser = argparse.ArgumentParser(description='ceramic package stl creator')
    parser.add_argument('short_side', type=int, help='short side of rectangular parallelepiped')
    parser.add_argument('long_side', type=int, help='long side of rectangular parallelepiped')
    parser.add_argument('height', type=int, help='height of rectangular parallelepiped')
    parser.add_argument('output', type=str, help='output directory')

    return parser.parse_args()


if __name__ == '__main__':
    main()
