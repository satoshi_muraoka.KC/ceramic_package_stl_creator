# ceramic package stl creator
コマンドライン引数で立方体の短辺の長さ、長辺の長さ、高さを指定して実行する。
```bash
# 10×24×5 の立方体を作る場合
python3 ceramic_package_stl_creator.py 10 24 5
```

短辺と長辺の中央が原点に、オブジェクト底面がｘｙ平面（ｚ＝０）に一致するように作成される。<br>

作成されたSTLファイルはoutputディレクトリにファイル名 cube_{短辺の長さ}_{長辺の長さ}_{高さ}.stl として出力される。<br>

