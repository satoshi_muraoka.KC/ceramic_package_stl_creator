#!/usr/bin/env python
import numpy as np
from itertools import combinations_with_replacement
from fractions import Fraction


def main():
    combination_list = list(combinations_with_replacement(range(3, 25), 2))
    aspect_ratios = []
    for l1, l2 in combination_list:
        # 最小化された形式で比を取得
        ratio = Fraction(l1, l2)
        # 結果をリストに追加
        aspect_ratios.append(ratio)

    # 重複した値を削除
    aspect_ratios = list(set(aspect_ratios))

    # ソート
    aspect_ratios.sort()

    # 結果の表示
    for ratio in aspect_ratios:
        print(f'{ratio.numerator}:{ratio.denominator}')


if __name__ == '__main__':
    main()
